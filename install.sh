#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-20
#
################################################################################
#set -x

# SOURCE
# ------
# https://docs.docker.com/install/linux/docker-ce/ubuntu/#upgrade-docker-after-using-the-convenience-script

# Uninstall old versions
sudo apt-get remove docker docker-engine docker.io

# Install Docker Community edition
sudo apt-get update

## Install dependancies
sudo  apt-get install --install-recommends \
      apt-transport-https \
      ca-certificates \
      curl \
      software-properties-common

# Add Docker's official GPG Key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add Docker's official repository for x86_64 and amd64
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

# Install Docker Community edition
sudo apt-get update
sudo apt-get install --install-recommend docker-ce

# Test Docker's install
sudo docker run hello-world

# Add User to Docker's group
sudo usermod -a -G docker $USER
echo "You must logout and login to apply modifications"

#set +x
exit 0
